import React, { Component } from 'react';

class utilisateurService {
    login = (pseudo, mdp) => {
        console.log(pseudo , mdp)
        let user = {pseudo : pseudo, mdp : mdp}
        const requestOptions = {
            method : 'POST',
            headers : {'Content-Type': 'application/json'},
            body: JSON.stringify(user)
        }
        fetch('http://localhost:3333/api/utilisateurs/login', requestOptions)
            .then(console.log('OK'))
    }
}

export default Object.freeze(new utilisateurService());