import React, {useState, Fragment} from 'react';
import utilisateurService from './Services/utilisateurService';

function App() {
  const [pseudo,setPseudo] = useState("");
  const [mdp,setMdp] = useState("");
  const [auth,setAuth] = useState({})

  const handleChangePseudo = (event) => {
    setPseudo(event.target.value);
  }

  const handleChangeMdp = (event) => {
    setMdp(event.target.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    setAuth({pseudo : {pseudo}, mdp : {mdp}})
    utilisateurService.login(pseudo, mdp)
  }

  const handleClickBtn = (event) => {
    event.preventDefault();
    
  }

  return (
    <Fragment>
    <form onSubmit={handleSubmit}>
      <label>login :</label>
        <input type='text' value={pseudo} onChange={handleChangePseudo}></input>
      <label>mdp :</label>
        <input type='text' onChange={handleChangeMdp}></input>
        <button>Clique</button>
    </form>
    <button onClick={handleClickBtn}>Clique ici après</button>
    </Fragment>
  );
}

export default App;
